/**
 * Created by aldri on 17/11/2015.
 */
const gulp            = require('gulp');
const _               = require('underscore');
const mainBowerFiles  = require('gulp-main-bower-files');
const rename          = require('gulp-rename');
const del             = require('del');
const fse             = require('fs-extra');
const path            = require('path');
/*
  Javascript
*/
const coffee    = require('gulp-coffee');
const coffeeson = require('coffeeson');
const ngAnnotate= require('gulp-ng-annotate');
const uglify    = require('gulp-uglify');
const concat    = require('gulp-concat');
const sourcemap = require('gulp-sourcemaps');
/*
  CSS
 */
const sass      = require('gulp-ruby-sass');
const prefixer  = require('gulp-autoprefixer');
const minifyCSS = require('gulp-minify-css');
/*
  HTML Template
*/
const jade = require('gulp-jade');
/*
  Essential
 */
const plumber = require('gulp-plumber');

/*
  NodeJS
*/
var nodeSrc = ['!ng-app/**','!node_modules/**','!bower_components/**','**/*.coffee'];
gulp.task('node-js-scripts',function(){
  gulp.src(nodeSrc)
      .pipe(plumber())
      .pipe(coffee({bare: true}))
      .pipe(gulp.dest('../dist'));
});

var angularSrc = [];
angularSrc.push('ng-app/configs/**/*.coffee');
angularSrc.push('ng-app/autoload/**/*.coffee');
angularSrc.push('ng-app/services/**/*.coffee');
angularSrc.push('ng-app/directives/**/*.coffee');
angularSrc.push('ng-app/controllers/**/*.coffee');
gulp.task('angular-js-scripts',function(){
  gulp.src(angularSrc)
      .pipe(plumber())
      .pipe(coffee({bare:true}))
      .pipe(ngAnnotate({single_quotes:true}))
      .pipe(sourcemap.init({loadMaps: true}))
      .pipe(concat('angular.js'))
      .pipe(uglify())
      .pipe(sourcemap.write())
      .pipe(gulp.dest('../dist/static/scripts'));
});

var sassConfig = {
  src :"ng-app/assets/sass/template/**/*.sass",
  dest:"../dist/static/styles"
};
gulp.task('sass',function(){
  sass(sassConfig.src)
    .pipe(plumber())
    .on('error',sass.logError)
    .pipe(prefixer())
    .pipe(minifyCSS())
    .pipe(gulp.dest(sassConfig.dest));
});

var jadeSrc = ['ng-app/**/*.jade'];
gulp.task('jade',function(){
  gulp.src(jadeSrc)
      .pipe(plumber())
      .pipe(jade())
      .pipe(gulp.dest('../dist/static'));
});

gulp.task('coffeeson',function(){
  gulp.watch("ng-app/lang/*.coffee",function(event){
    coffeeson.fileToJSON.pretty(event.path,function(err,json){
      var fileName  = path.parse(event.path);
      var dest      = '../dist/static/lang/' + fileName.name + '.json';
      del([dest]);
      fse.outputFile(dest,json,function(err){
      });
    });
  });
});

var tasks = [
  'node-js-scripts',
  'angular-js-scripts',
  'sass',
  'jade',
  'watch'
];

gulp.task('default',tasks);
gulp.task('watch',function(){
  gulp.watch(nodeSrc,[tasks[0]]);
  gulp.watch(angularSrc,[tasks[1]]);
  gulp.watch("ng-app/assets/sass/**/*.sass",[tasks[2]]);
  gulp.watch(jadeSrc,[tasks[3]]);
});

/*
  Bower Files
*/
gulp.task('load-bower-files',function(cb){
  var overrides = {
    uikit:{
      main:[
        './css/uikit.css',
        './css/components/tooltip.css',
        './css/components/datepicker.css',
        './css/components/autocomplete.css',
        './css/components/sticky.css',
        './css/components/notify.css',
        './js/uikit.js',
        './js/components/tooltip.js',
        './js/components/datepicker.js',
        './js/components/timepicker.js',
        './js/components/autocomplete.js',
        './js/components/sticky.js',
        './js/components/notify.js'
      ]
  }};

  gulp.src('./bower.json')
      .pipe(mainBowerFiles({overrides:overrides}))
      .pipe(gulp.dest('vendors'));
  cb()
});

gulp.task('transfer-assets',['load-bower-files'],function(cb){
  del('../dist/styles/fonts/**')
  gulp.src("vendors/**/*.{ttf,otf,woff,woff2}")
  .pipe(rename({dirname:''}))
  .pipe(gulp.dest('../dist/static/fonts'))

  del('../dist/images/**')
  gulp.src("vendors/**/*.{png,jpg,jpeg,svg}")
  .pipe(rename({dirname:''}))
  .pipe(gulp.dest('../dist/static/images'));
  cb()
});

gulp.task('create-dependencies',['transfer-assets'],function(cb){
  /*
    Write the Dependencies and concat them
  */
  var vendorDest    = 'vendors/';
  var JSdependencies = {
    foundations: [
      vendorDest + 'jquery/dist/jquery.js',
      vendorDest + 'moment/moment.js',
      vendorDest + 'underscore/underscore.js',
      vendorDest + 'leaflet/dist/leaflet-src.js'
    ],
    uiKit : [
      vendorDest + 'uikit/js/uikit.js',
      vendorDest + 'uiKit/js/components/tooltip.js',
      vendorDest + 'uikit/js/components/datepicker.js',
      vendorDest + 'uikit/js/components/autocomplete.js',
      vendorDest + 'uikit/js/components/timepicker.js',
      vendorDest + 'uikit/js/components/sticky.js',
      vendorDest + 'uikit/js/components/notify.js'
    ],
    angular : [
      vendorDest + 'angular/angular.js',
      vendorDest + 'angular-route/angular-route.js',
      vendorDest + 'angular-sanitize/angular-santize.js',
      vendorDest + 'angular-animate/angular-animate.js',
      vendorDest + 'angular-cookies/angular-cookies.js',
      vendorDest + 'ng-tags-input/ng-tags-input.min.js',
      vendorDest + 'angular-facebook/lib/angular-facebook.js',
      vendorDest + 'nginfinitescroll/build/ng-infinite-scroll.js',
      vendorDest + 'angular-simple-logger/dist/angular-simple-logger.js',
      vendorDest + 'angular-translate/angular-translate.js',
      vendorDest + 'angular-translate-handler-log/angular-translate-handler-log.js',
      vendorDest + 'angular-translate-loader-static-files/angular-translate-loader-static-files.js'
    ]
  };
  var JSdependenciesFlat = _.flatten([
    JSdependencies.foundations,
    JSdependencies.uiKit,
    JSdependencies.angular
  ]);
  gulp.src(JSdependenciesFlat)
      .pipe(plumber())
      .pipe(ngAnnotate({single_quotes:true}))
      .pipe(concat('js-dependencies.js'))
      .pipe(uglify())
      .pipe(gulp.dest('../dist/static/scripts'));

  var cssDependencies = [
    vendorDest + 'uikit/css/uikit.css',
    vendorDest + 'uikit/css/components/tooltip.css',
    vendorDest + 'uikit/css/components/datepicker.css',
    vendorDest + 'uikit/css/components/autocomplete.css',
    vendorDest + 'uikit/css/components/sticky.css',
    vendorDest + 'uikit/css/components/notify.css',
    vendorDest + 'leaflet/dist/leaflet.css'
  ];
  gulp.src(cssDependencies)
      .pipe(plumber())
      .pipe(concat('css-essentials.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('../dist/static/styles'))
  cb()
});

gulp.task('clean-bower',['create-dependencies'],function(cb){
  del('vendors/**');
});

gulp.task("install-dependencies",['load-bower-files','create-dependencies']);
