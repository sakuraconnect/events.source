express         = require 'express'
bodyParser      = require "body-parser"
cookieParser    = require "cookie-parser"
sessions        = require "express-session"
csrf            = require "csurf"
path            = require "path"
favico          = require "serve-favicon"
app             = express()

# Loging Mechanisim
morgan          = require "morgan"
fse             = require "fs-extra"
fsr             = require "file-stream-rotator"
moment          = require "moment"

MongoSessionStore = require('connect-mongodb-session')(sessions)
sessionStore      = new MongoSessionStore {
  uri         :'mongodb://127.0.0.1:27017/eventsDB'
  collection  :'Sessions'
}

sessionStore.on 'error', (err) ->
  console.error err

ctrDir = path.join __dirname,'api','controllers'
apiAuthenticate   = require path.join ctrDir,'authenticate'
apiDashboard      = require path.join ctrDir,'dashboard'
apiUsers          = require path.join ctrDir,'users'
apiSearch         = require path.join ctrDir,'search'
apiOrganizers     = require path.join ctrDir,'organizers'
apiEvents         = require path.join ctrDir,'events'
apiOptions        = require path.join ctrDir,'options'

secret = "c]]/lxjG|*4?]/*ET-s||W%?W/dZ>y_3r9X/ FI_d:^2]S$hK._blDLot7t*]ZEL"

# Favico
app.use favico path.join __dirname,'static','images','favico.png'

# Load Static files
app.use express.static path.join __dirname,'static'
# Load Static images
app.use '/uploaded-images',express.static __dirname + '/uploaded-images'
# Load Cookie Parser
app.use cookieParser secret
#Load session store
app.use sessions {
  secret: secret
  store : sessionStore
  resave: false
  saveUninitialized: false
}
#Load POST data parser
#form sent should be in JSON format
app.use bodyParser.json()

###
  Apis that do not need csrf
###
app.use '/api/search/',apiSearch

app.use csrf()
app.use (req,res,next) ->
  res.cookie 'XSRF-TOKEN',req.csrfToken()
  next()

###
  API Routes
###
app.use '/api/authenticate/',apiAuthenticate
app.use '/api/dashboard/',apiDashboard
app.use '/api/users/',apiUsers
app.use '/api/organizers/',apiOrganizers
app.use '/api/events/',apiEvents
app.use '/api/options/',apiOptions

###
  Logging
###
logdate = moment()
logDir  = path.join __dirname,'logs',logdate.format("YYYY"),logdate.format("MM")
fse.mkdirs logDir,(err) ->
  accessLogStream = fsr.getStream {
    filename    : path.join logDir,'%DATE%.log'
    frequency   : 'daily'
    verbose     : false
    date_format : 'DD'
  }
  app.use morgan 'combined',
    stream: accessLogStream


app.listen(3000);
