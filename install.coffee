dbConf  = require("config").get("db-settings")
MongoDB = require "mongojs"
_       = require "underscore"
yesno   = require "yesno"

DB          = MongoDB dbConf.database
collection  = DB.collection dbConf.tables.options

contactOptions = [
  "Twitter"
  "Instagram"
  "Website"
  "Email"
]

orgRoles = [
  "organizer"
  "supporting"
  "sponsor"
]

loadTags = [
  'cosplay'
  'figma'
  'anime'
  'japan pop-culture'
  'japan culture'
  'manga'
  'korean culture'
  'korean pop-culture'
  'charity'
  'gaming'
  'local comics'
  'doujin'
  'book event'
  'movie event'
  'esports'
]

oeEventFBId = "435500213215093"

###
  Check if the Collection is set
###
DB.getCollectionNames (err,doc) ->
  if err then console.log err
  isOptions = _.contains doc,dbConf.tables.options
  if not isOptions
    installDefaults()
    process.exit()
  else
    yesno.ask "Current settings will be overwritten!!! (Y/N)",false,(answer) ->
      if answer
        collection.remove {}, (err,doc)->
          console.log "Options remove"
          installDefaults()
          process.exit()
      else
        console.log "Settings not modified"
        process.exit()

installDefaults = ->
  bulk = collection.initializeOrderedBulkOp()
  bulk.insert {
    option: 'otaku-events-id'
    value : oeEventFBId
  }
  bulk.insert {
    option: 'contacts'
    value : contactOptions
  }
  bulk.insert {
    option: 'org-roles'
    value : orgRoles
  }
  bulk.insert {
    option: "tags"
    value : loadTags
  }
  bulk.execute (err,res) ->
