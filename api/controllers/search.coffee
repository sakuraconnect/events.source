app     = require "express"
router  = app.Router()

model = require "../models/search.js"

router.post "/venue", (req,res) ->
  model.venue req.body.query, (response) ->
    if response?
      res.send response
    else
      res.status(500).end()

router.post "/get-place-coords", (req,res) ->
  model.getCoords req.body.query, (response) ->
    if response?
      res.send response
    else
      res.status(500).end()

module.exports = router
