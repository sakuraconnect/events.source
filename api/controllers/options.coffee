app     = require "express"
_       = require "underscore"
router  = app.Router()

model = require "../models/options.js"

router.get "/list", (req,res) ->
  model.list req.query.option, (err,doc) ->
    if err then res.status(500).end()
    res.json doc

router.post "/edit",(req,res) ->
  if req.session.userInfo.role is not 0
    res.status(403).end()

  if _.isArray req.body
    model.editMany req.body, (err,doc) ->
      if err then res.status(500).end()
      res.end()

  if _.isObject req.body
    model.edit req.body, (err,doc) ->
      if(err) then res.status(500).end()
      res.end()

module.exports = router
