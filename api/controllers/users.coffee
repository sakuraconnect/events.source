app     = require "express"
router  = app.Router()

model   = require "../models/users.js"

router.post "/list", (req,res) ->
  # Check if the user is admin
  if req.session.userInfo.role is not 0
    res.status(403).end()

  model.listUsers (response) ->
    res.json response

router.post "/changerole", (req,res) ->
  # Make sure the user is not changing his role
  if req.session.userInfo.role is not 0 and req.body.fbid is not req.session.userInfo.fbid
    res.status(403).end()
  model.changeRole req.body
  res.status(200).end()

router.post "/deleteuser", (req,res) ->
  # Make sure the user is not deleteing itself
  if req.session.userInfo.role is not 0 and req.body.fbid is not req.session.userInfo.fbid
    res.status(403).end()

  model.deleteUser req.body, (response) ->
    if response.state is not "Error"
      res.status(200).end()
    else
      res.status(500).end()

router.post "/detail", (req,res) ->
  if req.session.userInfo?
    data =
      email: req.session.userInfo.email
      role : req.session.userInfo.role
    res.json data
  else
    res.status(202).end()

router.post "/edit", (req,res) ->
  if req.session.userInfo?
    data = req.session.userInfo
    data.newEmail = req.body.email
    model.edit data, (err,doc) ->
      if not err
        req.session.userInfo.email = req.body.email
        res.status(200).end()
      else
        res.status(500).end()
  else
    res.status(500).end()

module.exports = router
