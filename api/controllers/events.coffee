app     = require "express"
router  = app.Router()

model   = require "../models/events.js"

router.post "/add", (req,res) ->
  if req.session.userInfo.role > 500
    res.status(403).end()
  data            = req.body
  data.created_by = req.session.userInfo.fbid
  data.created_at = new Date()
  data.status     = "draft"

  model.add data, (doc) ->
    res.json doc

router.post "/manage/edit", (req,res) ->
  if req.session.userInfo.role > 500
    res.status(403).end()

  data = req.body
  data.created_by = req.session.userInfo.fbid
  data.status     = "draft"

  model.edit data,(doc) ->
    res.json doc

router.get "/list",(req,res) ->
  if req.session.userInfo.role > 500
    res.status(403).end()
  proj = {
    oe_event_id:1
    fb_event_id:1
    name:1
    schedule:1
    posterFiles:1
  }
  model.list proj,(doc) ->
    res.json(doc)

router.get "/manage/list", (req,res) ->
  if req.session.userInfo.role > 500
    res.status(403).end()

  proj = {
    name        :1
    posterFiles :1
    schedule    :1
    status      :1
    'loc.venue' :1
  }
  sort = {
    'schedule.from':-1
  }
  model.listMan proj,sort,(doc) ->
    res.json doc


router.post "/manage/change_status", (req,res) ->
  if(req.session.userInfo.role > 500)
    res.status(403).end()
  model.changeStatus req.body,(err,doc) ->
    res.status(200).end()

router.get "/manage/detail", (req,res) ->
  if req.session.userInfo.role > 500
    res.status(403).end()
  model.detailManage req.query, (err,doc) ->
    res.json(doc)

router.get "/index", (req,res) ->
  proj =
    schedule    : 1
    loc         : 1
    name        : 1
    posterFiles : 1
    slug        : 1
  model.index proj,(err,doc) ->
    if err then res.status(403).end()
    res.json doc

router.get "/detail", (req,res) ->
  model.detail req.query.slug, (err,doc) ->
    if err then res.status(500).end()
    res.json doc

module.exports = router
