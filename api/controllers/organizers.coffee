app     = require "express"
router  = app.Router()

model = require "../models/organizers.js"

router.post "/add", (req,res) ->
  data =
    id          : req.body._id 
    name        : req.body.name
    fb_page_id  : req.body.fb_page_id
    contacts    : req.body.contacts
    created_by  : req.session.userInfo.fbid

  model.add data, (doc) ->
    if doc.ok
      res.status(200).end()
    else
      res.status(500).end()

router.get "/list", (req,res) ->
  model.list (data) ->
    res.json data

router.post "/detail", (req,res) ->
  model.detail req.body, (data) ->
    res.json data

module.exports = router
