app     = require "express"
router  = app.Router()

model   = require "../models/authenticate.js"

router.post '/login', (req,res) ->
  model.authenticate req.body, (data) ->
    req.session.userInfo = data.payload
    res.json data

router.post '/session',(req,res) ->
  if req.session.userInfo?
    res.status(200).json(req.session.userInfo)
  else
    res.status(401).end()

router.post '/logout',(req,res) ->
  delete req.session.userInfo
  if not req.session.userInfo?
    res.status(299).end()
  else
    res.status(501).end()

module.exports = router
