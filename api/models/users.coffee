MongoDB = require "mongojs"
dbConf  = require("config").get("db-settings")
_       = require "underscore"

DB      = MongoDB dbConf.database
UsersDB = DB.collection dbConf.tables.users

module.exports =
  listUsers: (cb) ->
    _self = this
    UsersDB.find {}, (err,doc) ->
      response = {}
      if err
        response.state    = "Error"
        response.payload  = err
        cb response
      else
        response.state   = "Ok"
        response.payload = _self._formatPayload doc
        cb response

  changeRole: (data,cb) ->
    _self = this
    query =
      fbid: data.fbid
    update =
      "$set":
        role: data.role.role
    UsersDB.update query,update

  deleteUser: (data,cb) ->
    _self = this
    query =
      fbid: data.fbid

    UsersDB.remove query,true,(err,doc) ->
      response = {}
      if err
        response.state    = "Error"
        response.payload  = err
        cb response
      else
        response.payload = doc
        cb response

  edit: (userInfo,cb) ->
    console.log userInfo
    _self = this
    query =
      fbid: userInfo.fbid
    data =
      '$set':
        email: userInfo.newEmail
    UsersDB.update query,data,(err,doc) ->
      cb err,doc

  _formatPayload: (payload) ->
    _self = this
    # Set role property so that it can be read
		# by the angular ng-options track by set to role.id
    payload = _.each payload,(item) ->
      role      = item.role
      item.role =
        role:role
    return payload
