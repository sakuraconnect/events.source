MongoDB = require "mongojs"
dbConf  = require("config").get("db-settings")

request = require "request"
qs      = require "querystring"
DB      = MongoDB dbConf.database

apiKey = "AIzaSyB52mIz2RwwTfIrSwdw_CBVfFIGIr80cx4"

module.exports =
  venue: (query,cb) ->
    params =
      key   : apiKey
      input : query
    endpoint = "https://maps.googleapis.com/maps/api/place/queryautocomplete/json?"
    request endpoint + qs.stringify(params), (err,res,body) ->
      if err is null and res.statusCode is 200
        cb body
      else
        cb null

  getCoords: (query,cb) ->
    params =
      key     : apiKey
      placeid : query
    endpoint  = "https://maps.googleapis.com/maps/api/place/details/json?"

    request endpoint + qs.stringify(params), (err,res,body) ->
      if err is null and res.statusCode is 200
        cb body
      else
        null
