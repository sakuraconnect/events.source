MongoDB = require 'mongojs'
dbConf  = require('config').get('db-settings')
_       = require 'underscore'

DB          = MongoDB dbConf.database
collection  = DB.collection dbConf.tables.options

module.exports =
  list: (option,cb) ->
    _self = this
    query =
      option: option
    proj =
      value : 1
      _id   : 0

    collection.findOne query,proj,(err,doc) ->
      cb(err,doc)

  edit: (data,cb) ->
    _self = this
    query =
      option:option
    collection.update query,{'$set':{value:data.value}},(err,doc) ->
      cb(err,doc)

  editMany: (data,cb) ->
    _self = this
    _.each data,(val,key,list) ->
      _self.edit val,cb
