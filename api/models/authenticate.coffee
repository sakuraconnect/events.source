MongoDB = require "mongojs"
dbConf  = require("config").get("db-settings")

DB      = MongoDB dbConf.database
UsersDB  = DB.collection dbConf.tables.users

module.exports = {
  authenticate: (data,cb) ->
    record = {
      fbid        : data.id
      first_name  : data.first_name
      last_name   : data.last_name
      email       : data.email
      verified    : data.verified
      # 1000 = subscriber
      role        : 1000
      created_at  : new Date()
    }
    UsersDB.findOne {fbid:data.id}, (err,doc) ->
      response = {}
      if err
        response.state    = "Error"
        response.payload  = err
        cb(response)
      else
        if doc is null
          UsersDB.insert record, (err,doc) ->
            response.state    = "User Created"
            response.payload  = data
            cb(response)
        else
          response.state    = "User Auth"
          response.payload  = doc
          cb(response)
}
