MongoDB = require "mongojs"
dbConf  = require("config").get("db-settings")

DB          = MongoDB dbConf.database
collection  = DB.collection dbConf.tables.organizers

module.exports =
  dbCB : (err,doc,cb) ->
    _self = this
    if err then console.log err else cb doc

  add : (data,cb) ->
    _self = this
    if data.id?
      query =
        _id: MongoDB.ObjectId data.id
    else
      query =
        fb_page_id: data.fb_page_id
    update =
      name        : data.name
      fb_page_id  : data.fb_page_id
      contacts    : data.contacts
      created_at  : new Date()
      created_by  : data.created_by
    options =
      upsert: true

    collection.update query,update,options,(err,doc) ->
      _self.dbCB(err,doc,cb)

  list : (cb) ->
    _self   = this
    select  =
      name        : 1
      fb_page_id  : 1
      contacts    : 1
    collection.find({},select).sort {"name":1}, (err,doc) ->
      _self.dbCB err,doc,cb

  detail : (data,cb) ->
    _self = this
    query =
      _id: MongoDB.ObjectId data.id
    collection.findOne query, (err,doc) ->
      _self.dbCB(err,doc,cb)
