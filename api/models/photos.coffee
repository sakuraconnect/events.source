Jimp        = require "jimp"
randString  = require "randomstring"
moment      = require "moment"
fs          = require "fs-extra"
path        = require "path"
_           = require "underscore"
photoConf   = require("config").get("photo-settings")

Imagepath = photoConf['image-folder']
sizes =
  small : parseInt photoConf.small.res
  medium: parseInt photoConf.medium.res
  large : parseInt photoConf.large.res

quality = photoConf.quality

module.exports =
  resize: (url,cb) ->
    _self = this
    success = (url) ->
      # Set folder path
      d       = moment()
      year    = d.format("YYYY")
      month   = d.format("MM")
      dirPath = path.join Imagepath,year,month

      filename = _self.generateFile()
      filepath = {}

      # Make dir
      fs.mkdirs dirPath, (err) ->

        # Large
        trueFilePath  = path.join dirPath,'large-' + filename
        filepath.path = dirPath
        url.resize(sizes.large,Jimp.AUTO)
           .quality(quality)
           .write(trueFilePath)
        filepath.large = 'large-' + filename

        # Medium
        trueFilePath  = path.join dirPath,'medium-' + filename
        filepath.path = dirPath
        url.resize(sizes.medium,Jimp.AUTO)
           .quality(quality)
           .write(trueFilePath)
        filepath.medium = 'medium-' + filename

        # Small
        trueFilePath  = path.join dirPath,'small-' + filename
        filepath.path = dirPath
        url.resize(sizes.small,Jimp.AUTO)
           .quality(quality)
           .write(trueFilePath)
        filepath.small = 'small-' + filename

        cb(filepath)

    Jimp.read(url)
        .then(success)
        .catch (err) ->
          console.log err

  generateFile: ->
    _self   = this
    string  = randString.generate({
      length    : 10
      readable  : true
      charset   : 'alphanumeric'
    })
    return string + '.jpg'
