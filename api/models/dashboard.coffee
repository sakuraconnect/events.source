MongoDB = require "mongojs"
dbConf  = require("config").get("db-settings")

DB      = MongoDB(dbConf.database)
UsersDB = DB.collection(dbConf.tables.users)

module.exports =
  authenticate: (data,cb) ->
    record = {
      fbid        : data.id
      first_name  : data.first_name
      last_name   : data.last_name
      email       : data.email
      verified    : data.verified
      role        : 1000
      created_at  : new Date()
    }
    UsersDB.find {fbid:data.id}, (err,doc) ->
      response = {}
      if err
        response.state    = "Error"
        response.payload  = err
        cb(response)
      else
        if doc.length is 0
          UsersDB.insert record, (err,doc) ->
            response.state    = "Users Created"
            response.payload  = data
            cb(response)
        else
          response.state    = "User Authenticated"
          response.payload  = doc
          cb(response)
