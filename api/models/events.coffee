MongoDB = require "mongojs"
_       = require "underscore"
path    = require "path"
del     = require "del"
dbConf  = require("config").get("db-settings")

DB          = MongoDB dbConf.database
collection  = DB.collection dbConf.tables.events
photoMod    = require "./photos.js"
moment      = require "moment"

module.exports =
  convertPricetoInt: (price) ->
    _self = this
    parseInt price

  formatDate: (schedule) ->
    _self          = this
    normalizeDate  = moment schedule.date,"DD.MM.YYYY"
    dateString     = normalizeDate.format("YYYY-MM-DD") + " " + schedule.time + " " + schedule.zone

    normalizeDateTime = moment dateString,"YYYY-MM-DD HH:mm Z"
    return new Date normalizeDateTime.toISOString()

  dbCB: (err,doc,cb) ->
    _self = this
    if(err)
      console.log err
    else
      cb(doc)

  contacts: (data) ->
    _self = this
    _.each data.contacts, (val,key,list) ->
      if val.value.trim().length is 0
        data.contacts.splice key,1
    return data

  # Process Ticekts
  tickets: (data) ->
    _self = this
    _.each data.tickets, (val,key,list) ->
      if val.note.length > 0
        val.price = _self.convertPricetoInt val.price
        data.tickets[key].price = val.price
      else
        data.tickets.splice key,1
    return data

  #Schedule
  schedule: (data) ->
    _self = this
    eventDates = data.schedule
    delete data.schedule

    data.schedule =
      from: _self.formatDate eventDates.from
      to  : _self.formatDate eventDates.to

    return data

  add: (data,cb) ->
    _self = this
    addCB = (data) ->

      data = _self.tickets data
      data = _self.schedule data
      data = _self.contacts data

      delete data.posterUrl

      collection.insert data, (err,doc) ->
        _self.dbCB(err,doc,cb)

    if data.posterUrl?
      photoMod.resize data.posterUrl, (filePath) ->
        data.posterFiles =
          small : filePath.small
          medium: filePath.medium
          large : filePath.large
          date  : filePath.path
        addCB(data)
    else
      addCB(data)

  edit: (data,cb) ->
    _self = this

    data = _self.tickets data
    data = _self.schedule data
    data = _self.contacts data

    updateCB = (data) ->
      if data.orgPosterUrl?
        delete data.orgPosterUrl
      delete data.posterUrl

      query =
        _id: MongoDB.ObjectId data._id

      delete data._id
      collection.update query,data,(err,doc) ->
        _self.dbCB(err,doc,cb)

    if data.posterUrl is not data.orgPosterUrl and not data.orgPosterUrl? and data.posterUrl?
      if data.orgPosterUrl?
        # Delete current posterfiles
        types = ['small','medium','large']
        _.each types, (val) ->
          filePath = path.join data.posterFiles, data.posterFiles[val]
          del filePath

      photoMod.resize data.posterUrl, (filePath) ->
        data.posterFiles =
          small   : filePath.small
          medium  : filePath.medium
          large   : filePath.large
          date    : filePath.path
        updateCB data
    else
      updateCB data

  list: (proj,cb) ->
    _self = this
    collection.find {},proj,(err,doc) ->
      _self.dbCB err,doc,cb

  listMan: (proj,sort,cb) ->
    _self = this
    collection.find({},proj).sort sort,(err,doc) ->
      _self.dbCB err,doc,cb

  changeStatus: (eventInfo,cb) ->
    _self = this
    query =
      _id: MongoDB.ObjectId eventInfo._id

    data  =
      '$set':
        status: eventInfo.status
    options =
      upsert: true

    collection.update query,data,options, (err,doc) ->
      _self.dbCB err,doc,cb

  detailManage: (data,cb) ->
    _self = this
    query =
      _id: MongoDB.ObjectId data.id

    collection.findOne query, (err,doc) ->
      if err then return
      cb err,doc

  index: (proj,cb) ->
    _self = this

    collection.find {},proj, (err,doc) ->
      cb err,doc

  detail: (slug,cb) ->
    _self = this

    collection.findOne {slug:slug}, (err,doc) ->
      cb err,doc
