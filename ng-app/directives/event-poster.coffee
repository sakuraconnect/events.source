ngApp.directive 'eventPoster', ->
  returnMe = {
    restrict    :'E',
    replace     :true,
    scope       :
      event : '='
    templateUrl :'views/template/event-poster.html'
  }
