ngApp.directive 'oeEventList', ($rootScope,otakuEvents,helper) ->
  link = (scope,elem,attr) ->
    scope.nextPageUrl        = ''
    scope.loadingList        = true

    markAddedEvents = ->
      q = otakuEvents.addedEvents()
      q.then (res) ->
        angular.forEach res.data,(valMark,key) ->
          if valMark.oe_event_id.length > 0
            angular.forEach scope.OEList, (valList,key) ->
              if valList.id is valMark.oe_event_id
                scope.OEList[key].isAdded = true

    listOEEvents = ->
      q = otakuEvents.listEvents()
      q.then (res) ->
        if res.error?
          helper.notify res.error.message,"danger"
        else
          scope.loadingList = true
          scope.OEList = []
          scope.nextPageUrl = res.paging.next
          angular.forEach res.data,(val,key) ->
            val.profilePic = helper.formatFBPic val.id,'small'
            val.start_time = helper.formatTime val.start_time
            val.lastUpdate = helper.findDuration val.updated_time
            val.isPast     = helper.isPast val.end_time
            val.sortTime   = new Date(val.updated_time)
            scope.OEList.push val
          scope.loadingList = false
          markAddedEvents()
    listOEEvents()

    scope.listNextOEvents = ->
      scope.loadingList = true
      q = otakuEvents.PaginationlistEvents scope.nextPageUrl
      q.then (res) ->
        scope.loadingList = true
        scope.nextPageUrl = res.paging.next
        angular.forEach res.data,(val,key) ->
          val.profilePic = helper.formatFBPic val.id,'small'
          val.start_time = helper.formatTime val.start_time
          val.lastUpdate = helper.findDuration val.updated_time
          val.sortTime   = new Date(val.updated_time)
          scope.OEList.push val
          scope.loadingList = false

    scope.previewEvent = (fbid) ->
      $rootScope.$emit "facebookIdEventQuery",fbid
  returnMe = {
    restrict: 'E'
    replace  : true
    scope    : {
      fbGroup:'='
    }
    templateUrl:'views/template/admin-oe-list.html'
    link  : link
  }
