ngApp.directive 'dashboardNav',($rootScope,$window,api,helper) ->
  link = (scope,elem,attr) ->
    $rootScope.$watch 'authData', (newVal,oldVal) ->
      if newVal?
        scope.isAuthenticated = true
        scope.first_name  = $rootScope.authData.first_name
        scope.last_name   = $rootScope.authData.last_name
        if $rootScope.authData.fbid?
          scope.profilePic  = helper.formatFBPic($rootScope.authData.fbid,'square')
        else
          scope.profilePic  = helper.formatFBPic($rootScope.authData.id,'square')

        scope.isAdmin       = if newVal.role is 0 then true else false
        scope.isModerators  = if newVal.role <= 500 then true else false
      else
        scope.isAuthenticated = false

    #Delete the current session in the server
    #Delete the session stored in the front-end
    #Refresh the page
    scope.logout = ->
      q = api.postJSON('api/authenticate/logout',{})
      q.success (res) ->
        delete $rootScope.authData
        delete $rootScope.user
        $window.location.reload()

  returnMe = {
    restrict    : 'E'
    replace     : true
    scope       : {}
    templateUrl : 'views/template/dashboard-nav.html'
    link        : link
  }
