ngApp.directive 'sknMapForm', ($rootScope,helper) ->
  link = (scope,elem,attr) ->
    L.Icon.Default.imagePath = 'images'

    if not scope.location?
      scope.location      = {}
      scope.location.long = 121.05334
      scope.location.lat  = 14.62041

    marker = new L.marker([scope.location.long,scope.location.lat])
    #load Leaflet Map Container
    map = L.map(elem[0]).setView([scope.location.long,scope.location.lat], 15)
    map.addLayer(marker)

    scope.$watch 'location',(newValue,oldValue) ->
      if newValue?
        map.removeLayer(marker)
        marker = new L.marker([newValue.lat,newValue.long])
        map.addLayer(marker)
        map.setView([newValue.lat,newValue.long])

        #load Leaflet Map
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
          attribution:'Events Chart Powered by Open Streetmap'
          maxZoom: 18
        }).addTo(map)
  returnMe = {
    restrict:'E'
    replace  :true
    scope   :{
      location:'='
    }
    templateUrl:'views/template/skn-map-form.html'
    link  :link
  }
