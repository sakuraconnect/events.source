ngApp.directive "eventForm",($rootScope,api,helper,facebookSrv,eventInfoHelper,googleApi,$filter) ->
  link = (scope,elem,attr) ->
    $translate = $filter('translate')
    changeRegion = ->
      result = _.findWhere scope.regionOptions,{name:scope.eventInfo.loc.region}
      scope.areaOptions = result.area

    eventInfoHelper.loadLocation (rsp) ->
      scope.regionOptions = rsp.country.region
      scope.$apply()

    eventInfoHelper.loadContactOptions (options) ->
      scope.contactOptions = options

    eventInfoHelper.loadOrgRole (options) ->
      scope.orgRoles = options

    eventInfoHelper.loadTags (options) ->
      scope.tags = options

    scope.orgList         = []
    scope.disableSubmit   = false

    scope.findArea = (key,regionName) ->
      scope.eventInfo.loc.region = regionName
      searchArea = _.findWhere scope.regionOptions,{name:regionName}
      scope.areaOptions = searchArea.area

    scope.$watch 'regionOptions', (newValue,oldValue) ->
      if newValue?
        changeRegion()

    scope.changeRegion = ->
      changeRegion()

    scope.addContact = ->
      contactInfo = {name:'Twitter',value:""}
      scope.eventInfo.contacts.push contactInfo

    scope.addOrganizer = ->
      organizerInfo = {role:"Organizer",group:{}}
      scope.eventInfo.organizers.push organizerInfo

    scope.addTicket = ->
      ticketInfo = {price:0,note:""}
      scope.eventInfo.tickets.push ticketInfo

    scope.removeContact = (index) ->
      if scope.eventInfo.contacts.length > 1
        scope.eventInfo.contacts.splice index,1
      else
        scope.eventInfo.contacts[index].name = "Twitter"
        scope.eventInfo.contacts[index].value= ""
    scope.removeOrganizer = (index) ->
      if scope.eventInfo.organizers.length > 1
        scope.eventInfo.organizers.splice index,1
      else
        scope.eventInfo.organizers[index].role    = "Organizer"
        scope.eventInfo.organizers[index].group   = {}
    scope.removeTicket = (index) ->
      if scope.eventInfo.tickets.length > 1
        scope.eventInfo.tickets.splice index,1
      else
        scope.eventInfo.tickets[index].price = 0
        scope.eventInfo.tickets[index].note  = ""

    scope.searchVenue = ->
      q = googleApi.venue(scope.eventInfo.loc.venue)
      q.success (res) ->
        scope.venueSearchResult = res.predictions

    scope.getCoords = (placeid,name) ->
      q = googleApi.getCoords(placeid)
      q.success (res) ->
        result = res.result
        scope.eventInfo.loc = {
          venue   :name
          long    :result.geometry.location.lng
          lat     :result.geometry.location.lat
          region  :''
          area    :''
        }
        scope.venueSearchResult = []

    scope.addFbOrgId = ->
      scope.fbPageId = scope.eventInfo.fb_page_id

    scope.fetchOrgContacts = (orgId,key) ->
      contactList = _.findWhere scope.orgList,{fb_page_id:orgId}
      role = scope.eventInfo.organizers[key].role
      if role != "sponsor"
        angular.forEach contactList.contacts,(val,key) ->
          format = {name:val.name,value:val.value}
          scope.eventInfo.contacts.push format

    # Validation
    validateSubmitData = ->
      submitData    = scope.eventInfo
      scope.errors  = []

      # Check empty fields
      if not submitData.name? or not submitData.slug? or submitData.name.trim().length is 0 or submitData.slug.trim().length is 0
        helper.notify $translate('event-form.notif.blankField'),'danger'
        return false

      #Events need to be tied to a facebook event page
      if submitData.oe_event_id.trim().length is 0 and submitData.fb_event_id.trim().length is 0
        helper.notify $translate('event-form.notif.musthavefb')
        return false

      return true

    scope.submitForm = ->
      validationResult = validateSubmitData()
      angular.forEach scope.errors, (val,key) ->
        helper.notify val,'danger'

      if validationResult
        scope.disableSubmit   = true
        scope.onSubmit()

    scope.fbNameToId = (key) ->
      if scope.eventInfo[key].length > 0
        helper.notify $translate("org-manager.notif.fbtoname"),"primary"
        eventInfoHelper.fdNameToId scope.eventInfo[key], (res) ->
          console.log res
          if res.id?
            scope.eventInfo[key] = res.id
          else
            helper.notif $translate("org-manager.notif.fbtonameFail"),"danger"

    scope.setOrg = (key,val) ->
      scope.eventInfo.organizers[key].group = val
      scope.fetchOrgContacts val.orgid,key


    scope.$watchCollection "orgList", (newVal,oldVal) ->
      scope.orgNames = []
      angular.forEach scope.orgList, (val,key) ->
        scope.orgNames.push {name:val.name,orgid:val.fb_page_id}

    #Trigger this once the submit form is a success
    $rootScope.$on "onSubmitSuccess", (event) ->
      scope.disableSubmit       = false
      helper.notify "Event created",'success'

    $rootScope.$on "facebookIdQueryResult", (event,data) ->
      #Check if there is a place object in the result
      if data.place?
        venue   =   data.place.name + ", "
        if data.place.location?
          venue   +=  data.place.location.city + ", "
          venue   +=  data.place.location.country
          long    =   data.place.location.longitude
          lat     =   data.place.location.latitude
          country =   data.place.location.country
        else
          long    = 121.05334
          lat     = 14.62041
          country = ""
      else
        venue   = ""
        long    = 121.05334
        lat     = 14.62041
        country = "Philippines"

      #Make the Slug
      slug          = helper.string2slug data.name
      #Default OE Page
      OeEventsPage  = "435500213215093"
      #Parse the Date
      start_date    = moment(data.start_time)
      end_date      = moment(data.end_time)

      #Set where the event_id will be placed
      if data.owner.id is OeEventsPage
        fb_event_id = ""
        oe_event_id = data.id
        fb_page_id  = ""
      else
        fb_event_id = data.id
        oe_event_id = ""
        fb_page_id  = data.owner.id

      #wrap everything
      scope.eventInfo =
        name: data.name
        slug: slug
        loc :
          long    : long
          lat     : lat
          venue   : venue
          country : country
        oe_event_id: oe_event_id
        fb_event_id: fb_event_id
        fb_page_id : fb_page_id
        schedule:
          from:
            date: start_date.format "DD.MM.YYYY"
            time: start_date.format "HH:mm"
            zone: start_date.format "Z"
          to:
            date: end_date.format "DD.MM.YYYY"
            time: end_date.format "HH:mm"
            zone: end_date.format "Z"
        contacts: [
          {name:"Facebook",value:""}
        ]
        organizers: [
          {role:"Organizer",group:{}}
        ]
        tags:[]
        hashtags:[]
        tickets:[
          {price:0,note:""}
        ]

  returnMe =
    restrict: 'E'
    replace  : true
    scope  :
      eventInfo:'='
      onSubmit :'&'
    templateUrl:'views/template/event-form.html'
    link  : link
