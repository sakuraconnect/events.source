ngApp.directive "sponsorList", ($rootScope,helper,facebookSrv) ->
  link = (scope,elem,attr) ->
    scope.limit       = 3
    scope.currentPage = 0
    scope.pagination = (page) ->
      scope.currentPage = page
    scope.$watch "sponsors", (newValue,oldValue) ->
      if newValue?
        scope.maxPages = Math.ceil newValue.length / scope.limit
        batchCall = []
        angular.forEach newValue, (val,key) ->
          request =
            method      : 'GET'
            relative_url: val.orgid + '?fields=cover'
          batchCall.push request

        q = facebookSrv.bulkCall batchCall
        q.then (res) ->
          angular.forEach res, (val,key) ->
            if val.code is 200
              cover = JSON.parse val.body
              scope.sponsors[key].cover = cover.cover.source
  returnMe =
    link        : link
    restrict    : "E"
    replace     : true
    scope       :
      sponsors:'='
    templateUrl : 'views/template/sponsor-list.html'
