ngApp.directive "sknOrgManager",($rootScope,helper,eventInfoHelper,facebookSrv,api,$filter) ->
  link = (scope,elem,attr) ->
    $translate = $filter "translate"
    scope.facebookIdError   = false
    eventInfoHelper.loadContactOptions (options) ->
      scope.contactOptions = options

    scope.orgInfo =
      name        :''
      fb_page_id  :''
      contacts    :[]

    $rootScope.$on "fetchOrgList",(event) ->
      scope.orgList = if scope.orgList.length > 0 then [] else scope.orgList

      q = api.getJSON 'api/organizers/list',{}
      q.then (res) ->
        angular.forEach res.data, (val,key) ->
          format =
            _id         :val._id
            name        :val.name
            profile     :helper.formatFBPic val.fb_page_id,"square"
            fb_page_id  :val.fb_page_id
            contacts    :val.contacts
          scope.orgList.push format

    $rootScope.$emit "fetchOrgList"

    scope.fbNameToId = ->
      if scope.orgInfo.fb_page_id.length > 0
        helper.notify $translate("org-manager.notif.fbtoname"),"primary"
        eventInfoHelper.fdNameToId scope.orgInfo.fb_page_id, (res) ->
          if res.id?
            scope.orgInfo.fb_page_id = res.id
          else
            helper.notify $translate("org-manager.notif.fbtonameFail"),"danger"

    scope.contactSelected = (selectName,orgName) ->
      if selectName is orgName
        return true
      else
        return false

    scope.loadOrg = (id) ->
      data =
        id: id
      q = api.postJSON('api/organizers/detail',data)
      q.then (res) ->
        scope.orgInfo =
          _id         : res.data._id
          name        : res.data.name
          fb_page_id  : res.data.fb_page_id
          contacts    : res.data.contacts

    scope.getFBName = ->
      q = facebookSrv.api(scope.orgInfo.fb_page_id)
      q.then (res) ->
        scope.orgInfo.name = res.name

    scope.addContact = ->
      scope.orgInfo.contacts.push {name:"Facebook",value:""}
    scope.removeContact = (key) ->
      scope.orgInfo.contacts.splice key

    scope.submitOrg = ->
      contacts = []
      angular.forEach scope.orgInfo.contacts,(val,key) ->
        if val.value.trim().length > 0
          contacts.push {name:val.name,value:val.value}
      data =
        name        : scope.orgInfo.name
        fb_page_id  : scope.orgInfo.fb_page_id
        contacts    : contacts
      if scope.orgInfo._id?
        data._id = scope.orgInfo._id

      q = api.postJSON('api/organizers/add',data)
      q.then (res) ->
        if res.status is 200
          helper.notify $translate("org-manager.notif.added"),"success"
          $rootScope.$emit "fetchOrgList"
        else
          helper.notif $translate("org-manager.notif.fail"),"danger"

    scope.$watch 'fbPageId', (newValue,oldValue) ->
      if newValue? and newValue.trim().length > 0
        scope.orgInfo.fb_page_id = newValue
      else
        #helper.notify $translate("org-manager.notif.invalid"),"danger"
  returnMe =
    restrict    : 'E'
    replace     : true
    scope       :
      fbPageId:'='
      orgList :'='
    templateUrl : 'views/template/skn-org-manager.html'
    link        : link
