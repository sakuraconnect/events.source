ngApp.directive 'fbAddControl', ($rootScope,facebookSrv,helper) ->
	queryFBid = (req,cb) ->
		q = facebookSrv.apiScope(req)
		success = (res) ->
			cb(res)
		fail = (res) ->
			cb(res)
		q.then success,fail

	link = (scope,elem,attr) ->
		if attr.type is "event"
			fields = 'cover,name,start_time,end_time,owner,place'
			scope.type = attr.type
		else if attr.type is "page"
			fields = 'cover,name'

		$rootScope.$on 'facebookIdEventQuery', (event,data) ->
			scope.alertSuccess = false
			scope.fbId = data
			if scope.fbId?
				req = {
					url :'/' + data
					fields:{
						fields:fields
					}
				}
				queryFBid req, (res) ->
					if res and not res.error
						scope.fbQueryResult = res
						cover = if res.cover? then res.cover.source else '//placehold.it/800x600'
						scope.fbPreview = {
							cover	:cover
							name	:res.name
							profile :helper.formatFBPic res.id,'square'
						}
						scope.alertError = false
					else
						scope.alertError = true
			else
				scope.alertError = true
		scope.passQueryResult = ->
			$rootScope.$emit "facebookIdQueryResult", scope.fbQueryResult
			scope.alertSuccess = true
	returnMe = {
		restrict	:'E'
		replace	:true
		templateUrl:'views/template/dash-fb-addctrl.html'
		link	:link
	}
