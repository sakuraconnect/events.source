ngApp.directive "sknFbPhotoGallery", ($rootScope,helper,facebookSrv) ->
  batchImages = (batch,cb) ->
    q = facebookSrv.bulkCall batch
    q.then (res) ->
      imageList = []
      angular.forEach res, (val,key) ->
        if val.code is 200
          # body to json
          data    = JSON.parse val.body
          images  = _.sortBy data.images,'height'
          imageList.push {
            images: images
            url   : data.link
          }
      cb imageList

  loadGallery = (eventId,cb) ->
    if not eventId? then cb()
    req =
      url     : eventId+"/photos"
      fields  :
        fields: 'from'

    q = facebookSrv.apiScope req
    q.then (res) ->
      batch = []
      angular.forEach res.data,(value,key) ->
        request =
          method      : "GET"
          relative_url: value.id+"?fields=images,link"
        batch.push request

      # Bulk Batch Writing
      batchImages batch, (imageList) ->
        cb(imageList,res.paging.cursors.after)

  pageGallery = (eventId,nextPage,cb) ->
    req =
      url       : eventId + '/photos'
      fields    :
        fields:'from'
        after :nextPage
    photos = []
    q = facebookSrv.apiScope req
    q.then (res) ->
      batch = []
      angular.forEach res.data,(value,key) ->
        request =
          method      : "GET"
          relative_url: value.id+"?fields=images,link"
        batch.push request
      batchImages batch, (imageList) ->
        if res.paging? and res.paging.cursors.after?
           nextPage = res.paging.cursors.after
        else
           nextPage = null
        cb imageList, nextPage

  setButtonSelecttoFalse = (buttonSelected) ->
    angular.forEach buttonSelected, (value,key) ->
      buttonSelected[key] = false
    buttonSelected

  link = (scope,elem,attr) ->
    scope.photos = []
    OeEventsPage = "435500213215093"
    nextPage     = ""
    lastPage     = ""
    eventId      = ""

    scope.buttonSelected =
      oeGallery       : false
      fbEventGallery  : false
      fbPageGallery   : false
      customUrl       : false

    scope.paginationToggle = false

    scope.selectedPhoto = (photo) ->
      scope.photoDetail = photo
      scope.photoDetail.primaryImage = _.last photo.images

    scope.loadNextPage = ->
      scope.paginationToggle = false
      pageGallery eventId,nextPage,(res,pagination) ->
        if not pagination?
          scope.paginationToggle = true
        angular.forEach res,(value,key) ->
          nextPage = pagination
          scope.photos.push value

    scope.loadOEEventGallery = ->
      scope.photos = []
      scope.buttonSelected = setButtonSelecttoFalse scope.buttonSelected
      scope.buttonSelected.oeGallery     = true

      eventId = scope.eventInfo.oe_event_id
      if eventId?
        scope.facebookEventIdError = false
        loadGallery eventId, (res,pagination) ->
          scope.photos = res
          nextPage     = pagination
      else
        scope.facebookEventIdError = true

    scope.loadFBEventGallery = ->
      scope.photos = []
      scope.buttonSelected = setButtonSelecttoFalse scope.buttonSelected
      scope.buttonSelected.fbEventGallery = true

      eventId = scope.eventInfo.fb_event_id
      if eventId?
        scope.facebookEventIdError = false
        loadGallery eventId, (res,pagination) ->
          scope.photos = res
          nextPage     = pagination
      else
        scope.facebookEventIdError = true

    scope.loadFBPageGallery = ->
      scope.photos = []
      scope.buttonSelected = setButtonSelecttoFalse scope.buttonSelected
      scope.buttonSelected.fbPageGallery  = true

      eventId = scope.eventInfo.fb_page_id
      if eventId?
        scope.facebookEventIdError = false
        loadGallery eventId, (res,pagination) ->
          scope.photos   = res
          nextPage       = pagination
      else
        scope.facebookEventIdError = true

    scope.loadCustomUrl = ->
      scope.photos = []
      scope.buttonSelected = setButtonSelecttoFalse scope.buttonSelected
      scope.buttonSelected.customUrl = true

    scope.setPosterPhoto = ->
      scope.eventPoster = scope.photoDetail.primaryImage.source
      scope.posterSetAlert = true

    scope.$watch 'photoDetail.primaryImage.source', ->
      scope.posterSetAlert = false

  returnMe = {
    restrict: 'E'
    replace : true
    templateUrl:'views/template/skn-fb-photo-gallery.html'
    link  : link
    scope   : {
      eventInfo:'='
      eventPoster:"="
    }
  }
