ngApp.controller "mainCtrl",($scope,helper,$q,$rootScope,$location) ->
  def = $q.defer

  #List of URls that do not sync session data
  whiteListUrl = [
    '/authenticate'
  ]
  currentUrl = $location.url()
  if not _.contains(whiteListUrl,currentUrl)
    helper.syncSession()

  $rootScope.$on '$routeChangeStart',(event,next,current) ->
    $scope.displayNav = if $location.url() is '/authenticate' then false else true

  $rootScope.$on '$routeChangeError', ->
    console.error "User Not Authorized"
    $location.url '/dashboard/main'

  def.promise
