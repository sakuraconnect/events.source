ngApp.controller "loginCtrl",($scope,helper,facebookSrv,$rootScope,$location,api) ->
	helper.templateCss 'login.css'

	$scope.login = -> 
		facebookSrv.login()

	#Skip this if the user is already authenticated in the server
	if $rootScope.authData? 
		$location.url('/dashboard');

	#authenticate to mongo servers
	#Fire when facebookAuthenticate event is triggered
	$rootScope.$on "facebookAuthenticate", (event,data) ->
		api.postJSON('api/authenticate/login',data).then (res) ->
			$rootScope.authData 			= res.data.payload
			$location.url('/dashboard');