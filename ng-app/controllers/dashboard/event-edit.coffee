ngApp.controller 'eventsEditCtrl',($scope,$rootScope,helper,$routeParams,api) ->
  helper.templateCss "dashboard/admin-event-edit.css"

  #Get Event
  q = api.getJSON "api/events/manage/detail",{id:$routeParams.id}
  q.then (res) ->
    data = res.data

    from = {
      date: moment(data.schedule.from).format("DD.MM.YYYY")
      time: moment(data.schedule.from).format("HH:mm")
      zone: moment(data.schedule.from).format("Z")
    }
    to  = {
      date: moment(data.schedule.to).format("DD.MM.YYYY")
      time: moment(data.schedule.to).format("HH:mm")
      zone: moment(data.schedule.to).format("Z")
    }


    if data.posterUrl?
      convertFolder     = data.posterFiles.date.replace /\\/g,"/"
      data.posterUrl    = posterUrl
      data.orgPosterUrl = posterUrl
      posterUrl         = convertFolder + "/" + data.posterFiles.small

    #Convert dates for form parsing
    delete data.schedule.from
    delete data.schedule.to
    data.schedule.from = from
    data.schedule.to   = to

    $scope.eventInfo = data

  $scope.editEvent = ->
    cleanSubmitData = angular.toJson $scope.eventInfo
    console.log cleanSubmitData
    q = api.post "api/events/manage/edit",cleanSubmitData
    q.then (res) ->
      $rootScope.$emit "onSubmitSuccess"
