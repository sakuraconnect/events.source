ngApp.controller "eventsAddCtrl",($scope,$rootScope,helper,api,eventInfoHelper) ->
  helper.templateCss "dashboard/admin-event-add.css"

  $scope.eventInfo = eventInfoHelper.blankEventInfo();

  $scope.addEvent = () ->
      cleanSubmitData = angular.toJson $scope.eventInfo,0
      q = api.post "api/events/add", cleanSubmitData
      q.then (res) ->
        $rootScope.$emit "onSubmitSuccess"
        $scope.eventInfo = eventInfoHelper.blankEventInfo()
