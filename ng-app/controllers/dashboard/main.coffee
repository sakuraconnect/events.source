ngApp.controller "dashboardMainCtrl",($scope,$rootScope,helper,api) ->
  helper.templateCss 'dashboard/main.css'

  q = api.getJSON "api/events/index"
  q.then (res) ->
    $scope.events = []
    angular.forEach res.data, (val,key) ->
      if not val.posterFiles?
        val.posterUrl = "images/poster-default-small.png"
      else
        convertFolder = val.posterFiles.date.replace /\\/g,"/"
        url = convertFolder + "/" + val.posterFiles.small
        val.posterUrl = url
      startTime = moment(val.schedule.from).format('ll')
      val.startTime = startTime

      $scope.events.push val
