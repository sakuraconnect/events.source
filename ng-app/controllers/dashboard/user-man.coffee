#User Management Controller. FOR ADMIN ROLE ONLY
ngApp.controller "dashUserManagement", ($rootScope,$scope,helper,api) ->
	helper.templateCss 'dashboard/user-man.css'
	$scope.currentUser = $rootScope.authData.fbid;
	$scope.roleList = [
			{role:0,name:'Administrator'}
			{role:500,name:'Moderator'}
			{role:750,name:'Contributor'}
			{role:1000,name:'Subscriber'}
		]
	$scope.changeRole = (data) ->
		q = api.postJSON('api/users/changerole',data)

	$scope.deleteUser = (key,data) ->
		q = api.postJSON('api/users/deleteuser',data);
		q.success (res) ->
			$scope.userList.splice key,1;

	$scope.checkUser = (userData) ->
		$scope.userInfo 			= userData
		$scope.userInfo.profilePic 	= helper.formatFBPic userData.fbid,"large"

	userData = api.postJSON('api/users/list',{})
	userData.success (res) ->
		$scope.userList = res.payload
	userData.error (res) ->
		console.error res