ngApp.controller "eventsManageCtrl",($scope,$rootScope,helper,api) ->
  helper.templateCss "dashboard/admin-event-man.css"
  $scope.eventList    = []
  $scope.eventOption  = [
    'draft'
    'publish'
  ]
  # Load List
  loadEventList = ->
    q = api.getJSON "api/events/manage/list",{}
    q.then (res) ->
      angular.forEach res.data,(val,key) ->
        if val.posterFiles?
          convertFolder = val.posterFiles.date.replace /\\/g,"/"
          url = convertFolder + "/" + val.posterFiles.small
        else
          url = "images/poster-default-small.png"
        from = moment(val.schedule.from,'YYYY-MM-DDTHH:mm:Z')
        to   = moment(val.schedule.to,'YYYY-MM-DDTHH:mm:Z')
        format = {
          _id       : val._id
          name      : val.name
          schedule  : {
            from : from.format("DD.MM.YYYY HH:mm Z")
            to   : to.format("DD.MM.YYYY HH:mm Z")
          }
          status    : val.status
          poster    : url
          venue     : val.loc.venue
        }
        $scope.eventList.push format
  loadEventList()

  #change list status
  $scope.changeEventStatus = (key,status) ->
    $scope.eventList[key].status = status
    cleanObjEvent = angular.toJson $scope.eventList[key],0
    q = api.post "api/events/manage/change_status",cleanObjEvent
    q.then (res) ->
      console.log res
