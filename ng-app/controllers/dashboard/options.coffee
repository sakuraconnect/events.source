ngApp.controller "optionsAdminCtrl", ($scope,$rootScope,helper,api,eventInfoHelper) ->
  helper.templateCss "dashboard/options.css"

  $scope.contactInfos = []
  $scope.orgRoles     = []
  $scope.tags         = []

  submitOption = (data) ->
    cleanSubmit = angular.toJson(data)
    q = api.post "api/options/edit",data
    q.success ->
      helper.notify "Selection Updated",'success'

  eventInfoHelper.loadContactOptions (data) ->
    angular.forEach data, (val,key) ->
      format = {'text':val.name}
      $scope.contactInfos.push format

  eventInfoHelper.loadOrgRole (data) ->
    angular.forEach data, (val,key) ->
      format = {'text':val}
      $scope.orgRoles.push format

  eventInfoHelper.loadTags (data) ->
    angular.forEach data, (val,key) ->
      format = {'text':val}
      $scope.tags.push format

  eventInfoHelper.loadFbList (data) ->
    $scope.fbEventListId = data.value

  $scope.submitEventList = ->
    data = {
      option: 'otaku-events-id'
      value : $scope.fbEventListId
    }
    cleanSubmit = angular.toJson(data)
    q = api.post "api/options/edit",cleanSubmit
    q.success ->
      helper.notify "Event List Updated",'success'

  $scope.submitSelection = ->
    scope   = ['contactInfos','orgRoles','tags']
    options = ['contacts','org-roles','tags']
    values  = []
    data    = []
    x       = 0
    angular.forEach scope, (val1,key1) ->
      data[x] = {}
      data[x].option = options[key1]
      data[x].value  = []
      angular.forEach $scope[val1], (val2,key2) ->
        data[x].value.push val2.text
      x++
    submitOption(data)
