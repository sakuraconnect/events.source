ngApp.controller "eventDetailCtrl", ($scope,helper,api,$routeParams) ->
  helper.templateCss "dashboard/event-detail.css"

  $scope.event = {}
  $scope.pages =
    info    : true
    posts   : false
    discuss : false

  resetPages = (cb) ->
    $scope.pages['info']    = false
    $scope.pages['posts']   = false
    $scope.pages['discuss'] = false
    cb()

  $scope.showPage = (key) ->
    resetPages ->
      $scope.pages[key] = true


  q = api.getJSON "api/events/detail",{slug:$routeParams.slug}
  q.then (res) ->
    data       = res.data
    posterUrls = {}
    if not data.posterFiles?
      posterUrls.thumb = "images/poster-default-small.png"
      posterUrls.full  = "images/poster-default.png"
    else
      convertFolder     = data.posterFiles.date.replace /\\/g,"/"
      posterUrls.thumb  = convertFolder + '/' + data.posterFiles.medium
      posterUrls.full   = convertFolder + '/' + data.posterFiles.large

    organizers = []
    sponsors   = []
    angular.forEach data.organizers, (val,key) ->
      if val.role != "sponsor"
        organizers.push
          name    : val.group.name
          profile : helper.formatFBPic val.group.orgid,"square"
          orgid   : val.group.orgid
      else
        sponsors.push
          name    : val.group.name
          profile : helper.formatFBPic val.group.orgid,"square"
          orgid   : val.group.orgid
    tickets = []
    angular.forEach data.tickets, (val,key) ->
      if val.note.trim().length > 0
        tickets.push
          price: val.price
          note : val.note

    contactsNames = []
    angular.forEach data.contacts, (val,key) ->
      contactsNames.push val.name
    contactsNames = _.uniq contactsNames,true
    contacts = []
    angular.forEach contactsNames, (val,key) ->
      if val is "Website"
        icon = 'globe'
      else
        icon = val.toLowerCase()
      format = []
      list = _.where data.contacts, {name:val}
      angular.forEach list, (val,key) ->
        if val.name is "Twitter"
          format.push
            link    : "https://twitter.com/" + val.value
            contact : val.value
        else if val.name is "Website"
          format.push
            link    : "//" + val.value
            contact : val.value
      contacts.push
        group   : val
        'class' : icon
        values  : format

    dateFrom = moment data.schedule.from
    dateTo   = moment data.schedule.to

    # Since events only last ofr 8 hours
    duration = parseInt dateTo.subtract(dateFrom,'days').hours() / 8

    $scope.event =
      name      : data.name
      posterUrls: posterUrls
      organizers: organizers
      tags      : data.tags
      hashtags  : data.hashtags
      location  : data.loc
      tickets   : tickets
      contacts  : contacts
      oeFBId    : data.oe_event_id
      fbEventId : data.fb_event_id
      fbPageId  : data.fb_page_id
      sponsors  : sponsors
      schedule  :
        from    : moment(data.schedule.from).format("llll")
        to      : moment(data.schedule.to).format("llll")
        duration: duration

    console.log $scope.event
