ngApp.controller "profileOptionsCtrl", ($scope,helper,api) ->
  helper.templateCss "dashboard/options.css"

  loadUserDetail = () ->
    q = api.post "api/users/detail",{}
    q.then (res) ->
      $scope.email = res.data.email
      $scope.role  = res.data.role
      $scope.hash  = ''

  loadUserDetail()

  $scope.saveEmail = () ->
    q = api.post "api/users/edit",{email:$scope.email}
    q.then (res) ->
      if res.status is 200
        helper.notify "Email updated","success"
      else
        helper.notify "Error in updating","danger"
