title: "Events DB"
login:
  credits: "Events DB is Powered by Otaku Events. Events DB is under Sakura Connect banner"
buttons:
  'login-to-fb': "Login To Facebook"
'main-navs':
  home           : "Events List"
  'events-manage':
    title     : 'Events Manage'
    selection :
      add       : 'Add'
      manage    : 'Manage'
      'org-man' : 'Org Manage'
  'admin-control':
    title     : 'Admin Controls'
    selection :
      users   : 'Users'
      options : 'Options'
  labs:
    title     : 'Labs'
    selection :
      'events-map': 'Event Map'
  profile:
    profile:'Profile'
    logout :'Logout'
    auth   :'Register / Login'
'event-man':
  headers:
    name         : 'Name'
    schedule_from: 'Schedule From'
    schedule_to  : 'Schedule To'
    venue        : 'Venue'
    poster       : 'Poster'
    status       : 'Status'
    controls     : 'Controls'
  controls:
    edit  : 'Edit'
    delete: 'Delete'
'event-form':
  notif:
    blankField: "Required feilds are not filled. Check Event Name or the event slug"
    musthavefb: "Events must be tied to either an Otaku Events Facebook event page, or to their official event page"
  fields:
    'fb-list' : 'Otaku Events Event ID'
    'fb-event': 'Official Facebook Event ID'
    name      : 'Event Name'
    slug      : 'Event Slug'
    loc       :
      title: 'Location'
      venue: 'Venue'
    poster    :
      title : 'Poster'
      button: 'Upload Poster'
    'fb-page' : 'Facebook Event Page'
    organizers:
      title     : "Organizers"
      button    : "Add Organizer"
      'org-man' : 'Organizer Manager'
    contacts  : 'Contact Information'
    schedule  :
      title     : 'Schedule'
      'start-at': 'Start At'
      'end-at'  : 'Start End'
    tags      : 'Tags'
    tickets   : 'Tickets'
    hashtags  : 'Hashtags'
    submit    : 'Submit'
'oe-list':
  notif:
    added   : 'Added'
    next    : 'Next'
    loading : 'Loading Otaku Events'
'add-fb-ctrl':
  title: 'Add Facebook'
  notif:
    invalid : "This is not a valid Facebook Page"
    added   : "Facebook Page Added"
"fb-photo-gallery":
  group:
    fblist  : "Otaku Events"
    fbevent : "Facebook Event"
    fbpage  : "Facebook Page"
    url     : "Custom Url"
  notif:
    invalid: "Page ID not valid"
  button:
    next  : 'Next'
    submit: 'Poster Set'
    view  : 'View Image'
"org-manager":
  notif:
    invalid         : "Invalid Facebook Page"
    'already-added' : "This Organizer is already added"
    added           : 'Organizer Added'
    fail            : "Organizer Failed to be Added"
    fbtoname        : "Validating Facebook Name"
    fbtonameFail    : "Invalid Facebook ID"
  fields:
    legend : "Organizer Information"
    name   : "Organizer Name"
    fbpage : "Facebook Page ID"
    contact: "Contact Information"
  button:
    add   : "Add Contact"
    submit: "Submit"
admin:
  options:
    group:
      var: "Global Variables"
      sel: "Default Selections"
    fields:
      legend:
        gb  : "Global Variables"
        sel : "Default Selection"
      fblist  : "Event FB List"
      orgRole : "Organizaion Roles"
      contacts: "Contacts"
      tags    : "Tags"
      save    : "Save"
  users:
    header:
      names: "Names"
      email: "Email"
      roles: "Roles"
      info : "Info"
      remove:"Remove"
    'user-info':
      dbid      : "Database ID"
      fbid      : "Facebook ID"
      verified  : "Verified"
      registered: "Registered"
'event-detail':
  nav:
    info    : "More Info"
    link    : "Articles"
    comment : "Discussion"
  titles:
    organizedby              : "Organized By"
    'filed-under'            : "Filed Under:"
    contact                  : "Contact"
    facebook                 : "Facebook"
    'official-fb-event-page' : "Official Event Page"
    'official-fb-list'       : "Otaku Events Page"
    'ticket-info'            : "Ticket Information"
    'hashtags'               : "Hashtags"
    'official-fb-page'       : "Official FB Page"
    'sponsors'               : "Sponsors"
  notif:
    'no-ticket': "No Ticket Information Available"
