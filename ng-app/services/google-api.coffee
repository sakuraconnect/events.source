ngApp.factory 'googleApi', (api,$rootScope,$q,helper) ->

  returnMe = {
    venue: (query) ->
      url = 'api/search/venue'
      q = api.postJSON(url,{query:query})
      q.success (res) ->
        if res.error_message?
          helper.notify res.error_message,'danger'
    getCoords: (query) ->
      url = 'api/search/get-place-coords'
      q   = api.postJSON(url,{query:query})
  }
