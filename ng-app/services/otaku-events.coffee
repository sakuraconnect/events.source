ngApp.factory 'otakuEvents', (facebookSrv, helper,$rootScope,$q,api) ->
  otakuEventsFBid = "435500213215093"
  returnMe = {
    listEvents: ->
      _self   = this
      def     = $q.defer()
      access  = facebookSrv.accessToken()
      access.then (accessToken) ->
        fields  = 'updated_time,start_time,end_time,name,id,place'
        url     = '/'+otakuEventsFBid+"/events?accessToken="+accessToken+'&limit=40'+"&fields="+ fields
        def.resolve facebookSrv.api(url)
      def.promise

    PaginationlistEvents: (url) ->
      _self   = this
      def     = $q.defer()
      q       = facebookSrv.api(url)
      q.then (res) ->
        def.resolve res
      def.promise

    ###
      Events Already Added in the list
    ###
    addedEvents: ->
      _self = this
      q = api.getJSON "/api/events/list",{}
  }
