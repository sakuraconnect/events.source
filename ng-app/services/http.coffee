ngApp.factory "api", ($http,$q) ->
  returnMe = {
    postJSON : (url,sendData) ->
      config = {
        transformRequest : (obj) ->
          JSON.stringify(obj)
      }
      q = $http.post url,JSON.stringify sendData,config
    getJSON : (url,sendData) ->
      config = {
        params:sendData
      }
      q = $http.get url,config
    post: (url,sendData) ->
      q = $http.post url,sendData
  }
