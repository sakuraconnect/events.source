ngApp.factory "eventInfoHelper", ($rootScope,$q,api,facebookSrv) ->
  returnMe = {
    fdNameToId: (fbName,cb) ->
        req =
          url   : '/'+ fbName
          fields: {}
        q = facebookSrv.apiScope req
        q.then (res) ->
          console.log res
          cb res

    loadContactOptions: (cb) ->
      q = api.getJSON 'api/options/list',{option:'contacts'}
      q.then (res) ->
        format = []
        angular.forEach res.data.value, (val,key) ->
            format.push {name: val}
        cb(format)

    loadOrgRole: (cb) ->
      q = api.getJSON 'api/options/list',{option:'org-roles'}
      q.then (res) ->
        cb(res.data.value)

    loadTags: (cb) ->
      q = api.getJSON 'api/options/list',{option:'tags'}
      q.then (res) ->
        cb(res.data.value)

    loadLocation: (cb) ->
      return $.getJSON "philippines.json",(res) ->
        cb(res)

    loadFbList: (cb) ->
      return $.getJSON "api/options/list",{option:'otaku-events-id'}, (res) ->
        cb(res)

    blankEventInfo: ->
       return {
         name         :''
         fb_event_id  :''
         oe_event_id  :''
         fb_page_id   :''
         slug         :''
         posterUrl    :''
         loc          :{
           long   : 121.05334
           lat    : 14.62041
           venue  : ""
           country: "philippines"
           region : "metro manila"
         }
         schedule     :{}
         tags         :[]
         hashtags     :[]
         organizers   :[]
         tickets      :[]
         contacts     :[]
       }
  }
