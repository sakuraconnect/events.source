ngApp.factory 'facebookSrv', ($rootScope,$q,Facebook) ->
  returnMe =
    watchLoginChange : ->
      _self = this
      Facebook.Event.subscribe 'auth.authResponseChange', (res) ->
        if res.status is 'connected'
          #_self.getUserInfo()
        else
          #Destroy Session
    getUserInfo   : (accessToken) ->
      _self   = this
      fields  = "first_name,last_name,email,verified"
      Facebook.api '/me?fields=' + fields, (res) ->
        if not res.error?
          $rootScope.$apply ->
            res.accessToken = accessToken
            $rootScope.user = _self.user = res
            console.log $rootScope.user
            $rootScope.$emit "facebookAuthenticate", $rootScope.user
        else
          console.log res
          console.error "You are not currently logged in to Facebook"
    login       : ->
      _self = this
      #prevent the FB.Login() error
      Facebook.getLoginStatus (response) ->
        if response.status is "connected"
          _self.getUserInfo(response.authResponse.accessToken);
        else
          responseCB = (res) ->
            if res.authResponse
              _self.getUserInfo()
            else
              console.error "User did not authenticate"
          fbScope = 'email,user_birthday,public_profile'
          Facebook.login responseCB,{scope:fbScope}

    logout: ->
      _self = this
      Facebook.logout (res) ->
        $rootScope.$apply ->
          $rootScope.user = _self.user = {}

    api: (url) ->
      _self = this
      def = $q.defer()
      Facebook.api url,(res) ->
        def.resolve res
      def.promise

    apiScope    : (req) ->
      _self = this
      def   = $q.defer()
      _self.accessToken().then (accessToken) ->
        req.fields.accessToken = accessToken
        Facebook.api req.url,req.fields, (res) ->
          if res and not res.error
            def.resolve res
          else
            def.reject res
      def.promise

    accessToken: ->
      _self = this
      def = $q.defer()
      Facebook.getLoginStatus (res) ->
        if res.status is 'connected'
          def.resolve res.authResponse.accessToken
        else
          def.reject ""
      def.promise

    bulkCall: (payload) ->
      _self = this
      def   = $q.defer()
      _self.accessToken().then (accessToken) ->
        batchRequest =
          access_token: accessToken
          batch: angular.toJson payload
        Facebook.api "/","POST", batchRequest, (res) ->
          def.resolve res
      def.promise
