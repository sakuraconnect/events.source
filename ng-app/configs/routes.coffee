ngApp.config ($routeProvider) ->
  viewPath     = 'views/'
  dashboardPath   = viewPath + 'dashboard/'

  getServerSession = ($http,cb) ->
    #pass the session data from server to front-end
        sessionUrl = 'api/authenticate/session'
        query = $http.post(sessionUrl,{})
        query.success (res) ->
          cb(res)
        #delete authData if the server returns a "User Not Authorized request"
        query.error (res) ->
          cb(res)

    #Check if the user is qualifed for the previlage
    checkServerAuth = ($http,$q,role) ->
      defer = $q.defer()
      getServerSession $http,(res) ->
        if res.status is 401
          defer.reject("User Not Authorized")
        else
          if res.role <= role
            defer.resolve()
          else
            defer.reject("User Not Authorized")
      return defer.promise

  #set the required previlage for the user
  checkisAdmin = ($http,$q) ->
    return checkServerAuth $http,$q,0

  isMod  = ($http,$q) ->
    return checkServerAuth $http,$q,500

  $rp = $routeProvider

  $rp.when '/',
    redirectTo:'/authenticate'

  $rp.when '/authenticate',
    controller  :'loginCtrl'
    templateUrl  :viewPath + 'login.html'

  $rp.when '/dashboard',
    redirectTo:'/dashboard/main'

  $rp.when '/dashboard/main',
    controller : 'dashboardMainCtrl'
    templateUrl: dashboardPath + 'main.html'

  $rp.when '/dashboard/admin/users',
    controller :'dashUserManagement'
    templateUrl: dashboardPath + 'user-man.html'
    resolve:
      #Throw Error User is not Admin
      isAdmin: ['$http','$q',checkisAdmin]


  $rp.when '/dashboard/admin/event/add',
    controller  :'eventsAddCtrl'
    templateUrl  :dashboardPath + 'event-add.html'
    resolve:
      isMod: ['$http','$q',isMod]

  $rp.when '/dashboard/admin/events/manage',
    controller  : 'eventsManageCtrl'
    templateUrl : dashboardPath + 'event-man.html'
    resolve     :
      isMod: ['$http','$q',isMod]

  $rp.when '/dashboard/admin/event/edit/:id',
    controller : 'eventsEditCtrl'
    templateUrl: dashboardPath + 'event-edit.html'
    resolve    :
      isMod: ['$http','$q',isMod]

  $rp.when '/dashboard/admin/options',
    controller : 'optionsAdminCtrl'
    templateUrl: dashboardPath + 'options.html'
    resolve    :
      isMod:['$http','$q',checkisAdmin]

  $rp.when '/user/profile',
    controller : 'profileOptionsCtrl'
    templateUrl: viewPath + 'profile.html'

  $rp.when '/event/:slug',
    controller : 'eventDetailCtrl'
    templateUrl: dashboardPath + 'event-detail.html'

  $rp.when '/dashboard/admin/organizers',
    controller : 'organizersManageCtrl'
    templateUrl: dashboardPath + 'org-man.html'
