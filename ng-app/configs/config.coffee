extensions = [
	'ngRoute'
	'ngAnimate'
	'ngTagsInput'
	'facebook'
	'infinite-scroll'
	'nemLogging'
  'pascalprecht.translate'
]
ngApp = angular.module "events-db",extensions
