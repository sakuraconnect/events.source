ngApp.config ($translateProvider) ->
  staticFiles =
    prefix: '/lang/'
    suffix: '.json'
  $translateProvider.useStaticFilesLoader(staticFiles).preferredLanguage('en').useMissingTranslationHandlerLog()
