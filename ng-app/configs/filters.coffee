ngApp.filter 'startFrom', ->
  (input,start) ->
    if input?
      start = +start
      input.slice start

ngApp.filter 'range', ->
  (input,total) ->
    total = parseInt total

    for i in [0...total]
      input.push i

    input
